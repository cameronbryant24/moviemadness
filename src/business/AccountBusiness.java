package business;

import bean.User;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

/**
 * Session Bean implementation class AccountBusiness
 */
@Stateless
@Local(AccountBusinessInterface.class)
@LocalBean
@Alternative
public class AccountBusiness implements AccountBusinessInterface {

    /**
     * Default constructor. 
     */
    public AccountBusiness() {
    }

	/**
     * @see AccountBusinessInterface#AuthenticateUser(User)
     */
    public int AuthenticateUser(User user) {
        if(user.getPassword().equals("abcde")) {
        	return 1;
        } else { 
        	return 0; }
    }

	/**
     * @see AccountBusinessInterface#RegisterUsers(User)
     */
    public int RegisterUsers(User user) {
        // TODO When database is created. Connect this method to the DataAccess to put user in DB
			return 0;
    }

	/**
     * @see AccountBusinessInterface#hashPassword(String)
     */
    public String hashPassword(String pass) {
        // TODO When sending information to and from database, encrypt passwords here.
			return null;
    }

}
