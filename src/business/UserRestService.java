package business;

import javax.faces.bean.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import bean.User;

@RequestScoped
@Path("/users")
public class UserRestService {

	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public User getUser() {
		
		User user = new User();
		user.setFirstName("Cameron");
		user.setLastName("Bryant");
		return user;
	}
	
	@GET
	@Path("/get/{firstname}/{lastname}")
	@Produces(MediaType.APPLICATION_JSON)
	public User getUser(@PathParam("firstname") String firstName, 
						@PathParam("lastname") String lastName) {
		//This would typically call a business service to search for a user by for now just print a debug message a echo back a default user
		System.out.println("API was /getuser; I received request for name: " + firstName + " " + lastName);
		User user = new User();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		return user;
		//localhost:8080/appname/rest/users/get/cameron/bryant
		//if space in name include %20
	}
	
	@POST
	@Path("/save")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public User saveUser(User user) {
		//This would typically call a business service to save a user but for now just print a debug a meassage a echo back the user
		System.out.println("Hello " + user.getFirstName() + " " + user.getLastName());
		return user;
	}
	
}