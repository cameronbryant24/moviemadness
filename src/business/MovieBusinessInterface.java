package business;

import java.util.List;

import javax.ejb.Local;
import bean.Movie;

@Local
public interface MovieBusinessInterface {
	
	/**
	 * Call Data Access to put movies on the "shelf"
	 * @param repeat
	 * @return
	 */
	public int AddMovieToDatabase();
	
	/**
	 * Calls Data Access to remove the move from the "shelf"
	 * @return
	 */
	public int PurchaseMovie();
	
	/**
	 * Calls Data Access to move movie from "shelf" to user account.
	 * @return
	 */
	public int RentMovie();
	
	/**
	 * Returns a list of all movies by genre type
	 * @param genre
	 * @return
	 */
	public List<Movie> getMoviesByType();
	
}
