package business;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import bean.Movie;


/**
 * Session Bean implementation class MovieBusiness
 */
@Stateless
@Local(MovieBusinessInterface.class)
@LocalBean
public class MovieBusiness implements MovieBusinessInterface {

	private List<Movie> movies;
    /**
 	<class>business.MovieBusiness</class>
     * Default constructor. 
     */
    public MovieBusiness() {
    	movies = new ArrayList<Movie>();
        movies.add(new Movie("Dark Pheonix", 19.99f, 4.99f, "Action", "The X-Men face their most formidable and powerful foe when one of their own, "
        																+ "Jean Grey, starts to spiral out of control. During a rescue mission in outer space, "
        																+ "Jean is nearly killed when she's hit by a mysterious cosmic force. Once she returns home, "
        																+ "this force not only makes her infinitely more powerful, but far more unstable. The X-Men must "
        																+ "now band together to save her soul and battle aliens that want to use Grey's new abilities to "
        																+ "rule the galaxy.", "/resources/images/darkpheonix.jpg"));
        movies.add(new Movie("A Civil Action", 15.99f, 3.99f, "Action", "In this legal thriller based on a true story, John Travolta stars as Jan Schlichtmann, "
        																+ "a tenacious personal-injury attorney whose fierce determination entangles him in a case that "
        																+ "threatens to destroy him. The case -- which appears straight forward -- instead evolves into a "
        																+ "labyrinthine lawsuit of vast dimensions, in the intelligent, fast-paced drama", "/resources/images/acivilaction.jpg"));
        movies.add(new Movie("Matrix", 9.99f, 1.99f, "Action", "Neo (Keanu Reeves) believes that Morpheus (Laurence Fishburne), an elusive figure considered to be "
        																+ "the most dangerous man alive, can answer his question -- What is the Matrix? Neo is contacted by "
        																+ "Trinity (Carrie-Anne Moss), a beautiful stranger who leads him into an underworld where he "
        																+ "meets Morpheus. They fight a brutal battle for their lives against a cadre of viciously "
        																+ "intelligent secret agents. It is a truth that could cost Neo something more precious than his life.", "/resources/images/matrix.png"));
    }

	/**
     * @see MovieBusinessInterface#RentMovie()
     */
    public int RentMovie() {
        // TODO Auto-generated method stub
			return 0;
    }

	/**
     * @see MovieBusinessInterface#PurchaseMovie()
     */
    public int PurchaseMovie() {
        // TODO Auto-generated method stub
			return 0;
    }

	/**
     * @see MovieBusinessInterface#AddMovieToDatabase()
     */
    public int AddMovieToDatabase() {
        // TODO Auto-generated method stub
			return 0;
    }

	@Override
	public List<Movie> getMoviesByType() {
		// After Data Access is created, call to get a list of movies
		// based off of the genre.
		return movies;
	}

	
}
