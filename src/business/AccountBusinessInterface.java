package business;

import javax.ejb.Local;

import bean.User;

@Local
public interface AccountBusinessInterface{
	/**
	 * Hashes password and sends user to the Data Access
	 * layer to add user to the user table.
	 * @param user
	 * @return
	 */
	public int RegisterUsers(User user);
	
	/**
	 * Hash password, retrieve user from the Data Access
	 * layer to authenticate that the user is who they 
	 * say they are.
	 * @param user
	 * @return
	 */
	public int AuthenticateUser(User user);
	
	/**
	 * Hashes passwords for security measures.
	 * @param pass
	 * @return
	 */
	String hashPassword(String pass);
}
