package business;

import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import bean.Movie;

@RequestScoped
@Path("/orders")
public class OrdersRestService {
	@Inject
	MovieBusinessInterface service;
	
	@GET
	@Path("/getjson")
	@Produces(MediaType.APPLICATION_JSON)
	
	public List<Movie> getMoviesAsJson() {
		return service.getMoviesByType();
	}
	
	@GET
	@Path("/getxml")
	@Produces(MediaType.APPLICATION_XML)
	public Movie[] getMoviesAsXml() {
		List<Movie> movies = service.getMoviesByType();
		return movies.toArray(new Movie[movies.size()]);
	}

}
