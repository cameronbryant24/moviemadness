package data;

import java.util.List;

import javax.ejb.Local;

import exception.BillingAddressNotFoundException;

@Local
public interface DataAccessInterface <T>{

	public List<T> findAll();
	public T findById(int id) throws Exception;
	public boolean createT(T t);
	public boolean updateT(T t);
	public boolean deleteT(T t);
	
}
