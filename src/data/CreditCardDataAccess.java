package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import bean.CreditCard;
import bean.User;
import exception.CardNotFoundException;

@Stateless
@Local(DataAccessInterface.class)
@LocalBean
public class CreditCardDataAccess implements DataAccessInterface<CreditCard> {

	private Connection conn = null;
	private String url = "jdbc:mysql://localhost:3306/moviemadness";
	private String username = "Cameron";
	private String password = "123456";
	
	@Override
	public List<CreditCard> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CreditCard findById(int id) throws CardNotFoundException {
		try {
			User user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
			String sql = String.format("SELECT `CARD_ID` FROM `user` WHERE `USER_ID` = %d;", user.getUserID());
			
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			int cardId = rs.getInt("CARD_ID");
			
			
			String sql2 = String.format("SELECT * FROM `payment` WHERE `CARD_ID`= %d;", cardId);
			Statement stmt2 = conn.createStatement();
			ResultSet rs2 = stmt2.executeQuery(sql2);
			rs2.next();
			CreditCard c = new CreditCard();
			c.setCardId(rs2.getInt("CARD_ID"));
			c.setNumber(rs2.getString("CARD_NUMBER"));
			c.setNameOnCard(rs2.getString("NAME"));
			c.setExDate(rs2.getDate("EXPIRATION"));
			c.setCcv(rs2.getString("CCV"));
			c.setBillingAddressID(rs2.getInt("BILLING_ADDRESS"));
			
			stmt.close();
			stmt2.close();
			return c;
			
		} catch (SQLException e) {
			//e.printStackTrace();
			throw new CardNotFoundException();
		}finally {
		
			
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean createT(CreditCard t) {
		try {
			String sql = String.format("INSERT INTO `payment` (`CARD_NUMBER`, `NAME`, `EXPIRATION`, `CCV`) VALUES ('%s', '%s', DATE '%tF', '%s')",
					t.getNumber(),
					t.getNameOnCard(),
					t.getExDate(),
					t.getCcv());
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			
			String sql2 = "SELECT LAST_INSERT_ID() AS `LAST_ID` FROM `payment`;";
			ResultSet rs = stmt.executeQuery(sql2);
			rs.next();
			int cardID = rs.getInt("LAST_ID");
			stmt.close();
			User user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("cardId", cardID);
			Statement stmt2 = conn.createStatement();
			String sql3 = String.format("UPDATE `user` SET `CARD_ID` = %d WHERE `USER_ID` = %d;", cardID, user.getUserID());
			stmt2.executeUpdate(sql3);
			stmt2.close();
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean updateT(CreditCard t) {
		try {
			String sql = String.format("UPDATE `payment` SET `CARD_NUMBER` = '%s', `NAME` = '%s', `EXPIRATION` = DATE '%tF', `CCV` = '%s' WHERE `CARD_ID` = %d",
					t.getNumber(),
					t.getNameOnCard(),
					t.getExDate(),
					t.getCcv(),
					t.getCardId());
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean deleteT(CreditCard t) {
		// TODO Auto-generated method stub
		return false;
	}

}
