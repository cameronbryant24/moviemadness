package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import bean.Movie;

@Stateless
@Local(DataAccessInterface.class)
@LocalBean
public class MovieDataAccess implements DataAccessInterface<Movie> {

	private Connection conn = null;
	private String url="jdbc:mysql://localhost:3306/moviemadness";
	private String username = "Cameron";
	private String password = "123456";
	
	@Override
	public List<Movie> findAll() {
		String sql = "SELECT * FROM `movie` ORDER BY `GENRE`;";
		List<Movie> movies = new ArrayList<Movie>();
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				movies.add(new Movie(
						rs.getString("NAME"),
						rs.getFloat("PURCHASE_PRICE"),
						rs.getFloat("RENT_PRICE"),
						rs.getString("GENRE"),
						rs.getString("SUMMARY"),
						rs.getString("IMAGE_URI")
						));
			}
			return movies;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public Movie findById(int id) {
		String sql = String.format("SELECT * FROM `movie` WHERE `MOVIE_ID`= %i", id);
		
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			return null; //TODO return a movie
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean createT(Movie t) {
		try {
			String sql = String.format("INSERT INTO `movie` (`NAME`, `PURCHASE_PRICE`, `RENT_PRICE`, `GENRE`, `SUMMARY`, `IMAGE_URI`)"
					+ "VALUES ('%s', %f, %f, '%s', \"%s\", '%s');",
					t.getTitle(),
					t.getBuyPrice(),
					t.getRentPrice(),
					t.getGenre(),
					t.getSummary(),
					t.getImageURI());
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean updateT(Movie t) {
		String sql = String.format("");
		
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean deleteT(Movie t) {
		String sql = String.format("");
		
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public List<Movie> getByGenre(String genre){
		String sql = String.format("SELECT * FROM `movie` WHERE `GENRE` = '%s';", genre);
		List<Movie> movies = new ArrayList<Movie>();
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				movies.add(new Movie(
						rs.getString("NAME"),
						rs.getFloat("PURCHASE_PRICE"),
						rs.getFloat("RENT_PRICE"),
						rs.getString("GENRE"),
						rs.getString("SUMMARY"),
						rs.getString("IMAGE_URI")
						));
			}
			return movies;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public List<String> getGenreTypes(){
		String sql = "SELECT DISTINCT `GENRE` FROM `movie`;";
		List<String> genres = new ArrayList<String>();
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				genres.add(rs.getString("GENRE"));
			}
			return genres;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
