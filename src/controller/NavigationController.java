package controller;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import business.MovieBusinessInterface;

@ManagedBean
@ViewScoped
public class NavigationController {

	@EJB
	MovieBusinessInterface movieService;

	
	public MovieBusinessInterface getMovieService() {
		return movieService;
	}
	
}
