package controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.Part;
import bean.Movie;
import business.MovieBusiness;

@ManagedBean
@ViewScoped
public class MovieController {
	@Inject
	MovieBusiness movieService;
	
	private Part imageFile;
	private String imageDirectory = "C:\\Users\\natha\\EAP-7.1.0\\standalone\\deployments\\MovieMadness.war\\resources\\images";

	public Part getImageFile() {
		return imageFile;
	}

	public void setImageFile(Part imageFile) {
		this.imageFile = imageFile;
	}
	
	public void uploadImage() {
		try{
            InputStream in=imageFile.getInputStream();
            
            File f=new File(imageDirectory+imageFile.getSubmittedFileName());
            f.createNewFile();
            FileOutputStream out=new FileOutputStream(f);
            
            byte[] buffer=new byte[1024];
            int length;
            
            while((length=in.read(buffer))>0){
                out.write(buffer, 0, length);
            }
            
            out.close();
            in.close();
            
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("path", f.getAbsolutePath());
            
        }catch(Exception e){
            e.printStackTrace();
        }
	}
	
	public String addMovie(Movie m) {
		uploadImage();
		m.setImageURI(imageFile.getSubmittedFileName());
		movieService.AddMovieToDatabase();
		return "BrowseAll.xhtml";
	}
	
	public MovieBusiness getMovieService() {
		return movieService;
	}
	
	
	
}
