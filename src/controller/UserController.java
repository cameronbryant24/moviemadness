package controller;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import bean.User;
import business.AccountBusinessInterface;

@ManagedBean
@ViewScoped
public class UserController {

	@EJB
	AccountBusinessInterface ab;
	/**
	 * 
	 * @param user
	 * @return
	 */
	public String onLogin(User user) {
		//Get user model and redirect to the landing page
		if(ab.AuthenticateUser(user) == 1) {
			return "BrowseAll.xhtml";
		}
		else {
			return "Login.xhtml";
		}
		
	}
	
	/**
	 * 
	 * @param user
	 * @return
	 */
	public String onRegister(User user) {
		//Get user model and put it into the context map
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		//redirect user to login page
		return "Login.xhtml";
	}
}
