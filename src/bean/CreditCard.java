package bean;

import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

@ManagedBean
@ViewScoped
public class CreditCard {

	@NotBlank(message="Card Number Required")
	@Size(min=16, max=16)
	private String number;
	
	@NotNull(message="Date Required")
	@NotBlank(message="Date Required")
	private Date exDate;
	
	@NotBlank(message="CCV Required")
	@NotNull(message="CCV Required")
	private String ccv;
	
	@NotBlank(message="Name Required")
	private String nameOnCard;
	
	private int billingAddressID;
	
	private int CardId;
	
	public CreditCard() {
		super();
		this.number = "";
		this.exDate = new Date();
		this.ccv = "";
		this.nameOnCard = "";
		this.billingAddressID = -1;
		this.CardId = 0;
		
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Date getExDate() {
		return exDate;
	}

	public void setExDate(Date exDate) {
		this.exDate = exDate;
	}

	public String getCcv() {
		return ccv;
	}

	public void setCcv(String ccv) {
		this.ccv = ccv;
	}

	public String getNameOnCard() {
		return nameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	public int getBillingAddressID() {
		return billingAddressID;
	}

	public void setBillingAddressID(int billingAddressID) {
		this.billingAddressID = billingAddressID;
	}

	public Object getCardId() {
		return CardId;
	}
	
	public void setCardId(int CardId) {
		this.CardId = CardId;
	}
}
