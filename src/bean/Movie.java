package bean;


public class Movie {

	private String title;
	private float buyPrice;
	private float rentPrice;
	private String genre;
	private String summary;
	private String imageURI;
	
	public Movie() {
		super();
		this.title = "";
		this.buyPrice = 0.f;
		this.rentPrice = 0.f;
		this.genre = "";
		this.summary = "";
		this.imageURI = "";
	}

	public Movie(String title, float buyPrice, float rentPrice, String genre, String summary, String imageURI) {
		super();
		this.title = title;
		this.buyPrice = buyPrice;
		this.rentPrice = rentPrice;
		this.genre = genre;
		this.summary = summary;
		this.imageURI = imageURI;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public float getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(float buyPrice) {
		this.buyPrice = buyPrice;
	}

	public float getRentPrice() {
		return rentPrice;
	}

	public void setRentPrice(float rentPrice) {
		this.rentPrice = rentPrice;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getImageURI() {
		return imageURI;
	}

	public void setImageURI(String imageURI) {
		this.imageURI = imageURI;
	}
	
	
}
