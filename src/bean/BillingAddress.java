package bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.hibernate.validator.constraints.NotBlank;

@ManagedBean
@ViewScoped
public class BillingAddress {
	
	@NotBlank(message="Address Required")
	private String address1;
	
	private String address2;
	
	private String address3;
	
	@NotBlank(message="Country required")
	private String country;
	
	@NotBlank(message="City required")
	private String city;
	
	@NotBlank(message="State required")
	private String state;
	
	@NotBlank(message="Zip required")
	private String zip;
	
	private int BillingId;
	
	public BillingAddress() {
		super();
		this.address1 = "";
		this.address2 = "";
		this.address3 = "";
		this.country = "";
		this.city = "";
		this.state = "";
		this.zip = "";
		this.BillingId = 0;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Object getBillingId() {
		return BillingId;
	}
	
	public void setBillingId(int BillingId) {
		this.BillingId = BillingId;
	}
}
